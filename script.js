// script.js

document.addEventListener('DOMContentLoaded', () => {
    const statusCells = document.querySelectorAll('.status');

    statusCells.forEach(cell => {
        if (cell.textContent.trim().toLowerCase() === 'online') {
            cell.classList.add('online');
        } else {
            cell.classList.add('offline');
        }
    });
});
